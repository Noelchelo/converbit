<?php

/**
 * Clase Task
 */
class Task extends BaseTask
{
    /**
     * Initialize (Ejecutada de primera por el administrador de hilos)
     * 
     * @return mixed
     */
    public function initialize() 
    {
        return true;
    }

    /**
     * Ejecutada por el administrador de hilos si el proceso se completó con exito (Cuando el metodo process() haya retornado true)
     * 
     * @return mixed
     */
    public function onSuccess()
    {
        return true;
    }

    /**
     * Ejecutada por el administrador de hilos si el proceso se completó con fallos (Cuando el metodo process() haya retornado false)
     * 
     * @return mixed
     */
    public function onFailure() 
    {
        return false;
    }

    /**
     * Método principal que contiene la lógica a ser ejecutada por la tarea
     * 
     * @param $params array Array asociativo de parametros
     *
     * @return boolean True para Éxito, false de lo contrario
     */
    public function process(array $params = array())
    {
        $conexion = new conexion();
        $format = $params["format"];
	    $link = $params["link"];
        $name = $params["name"];
        $verinum = $params["verificationnumber"];
        $comand = "sudo youtube-dl -f $format -o '/var/www/html/downloads/$name.$format'  $link";
        //$comand = "sudo youtube-dl --recode-video $format -o '/var/www/html/downloads/$name'  $link";
        $url = "http://192.168.0.8/downloads/".$name.".".$format;
        echo "\n";
        echo "Iniciando descarga del vídeo ".$link;
        echo "\n";
        $resultado = exec($comand);
        $arr = explode(" ", $resultado);
        $do = $arr[0];
        $comp ="[download]";
        echo "\n";
        
        echo "\n";
        if (strpos($do, $comp) !== false) {
            echo "La descarga del vídeo ".$link. " se ha completado";
            echo "\n";
            $conexion->updateDownload( $name, "Finished", $url);
            //$conexion->addFiles($verinum,$link, $format, $url);
            return true;  
        }
        else{
            $conexion->error($name);
            echo "Imposible realizar la descarga de ese vídeo, debido a restricciones de la página...";
        }  
    }
}
