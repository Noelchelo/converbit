<?php
require 'vendor/autoload.php';
require ("Task/addBrokenLink.php");
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class receiveRabbit{

    public  $instanceConexion;
    public  $instanceBrokenLink;
   // public  $instancedownload;
    public function recive()
    { 
        $this->instanceConexion = new conexion();
        $this->instanceBrokenLink = new addBrokenLink(); 
       // $this->instancedownload = new download();

        $connection = new AMQPStreamConnection('192.168.0.10', 5672, 'admin', 'admin');
        $channel = $connection->channel();
        

        $channel->queue_declare(
            $queue = 'downloads',
            $passive = false,
            $durable = true,
            $exclusive = false,
            $auto_delete = false,
            $nowait = false,
            $arguments = null,
            $ticket = null
        );


        $callback = function($msg) {
            sleep(substr_count($msg->body, '.'));
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            $condition = json_decode($msg->body, true);
            //echo $msg->body;
            
            //echo var_dump($condition);
            
            if(empty($condition)){
                echo "no hay colas";
            }
            else{
                $this->instanceConexion->updateDownload($condition['id'], "Active", " ");
                //echo $condition['link'];    
                $array = [
                    "verificationnumber" => $condition['id'],
                    "format" => $condition['format'],
		            "link" => $condition['link'],
		            "name" => $condition['id']
                ];
                
                if($this->instanceBrokenLink->add($condition['link'],$condition['id'],$condition['format'])){
                    echo $condition['link'];
                    echo "Este es un enlace duro\n";
                }else{
                    
                    $Task = new Task();
                    $multithreadManager = new ThreadManager();
                    $multithreadManager->start($Task, $array);
                    //echo $condition['link'];
                    //echo "\n";
                }
                
            }
            
        };

        $channel->basic_qos(null,1, null);

        $channel->basic_consume('downloads', '', false, false, false, false, $callback);
        

        while ($channel->is_consuming()) {
            if($this->instanceConexion->getSpaces() < 5){  
                $channel->wait();
            }else{
                
            }
        }

        $channel->close();
        $connection->close();
    }

} 


?>
