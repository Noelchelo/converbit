<x-app-layout>
    <x-slot name="header">
    
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                
                <div class="card custom-card">
                            <div class="card-header custom-card-header">
                                Add url
                            </div>  
                    <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <!-- Here is ubicated the input-link of YT -->
                                <form action="{{ url('dashboard/store') }}" method="POST">
                                    <div class="input-group">
                                        @csrf
                                        <input name="link" type="text" class="form-control" placeholder="Link here ex: https://www.youtube.com/watch?v=XIAtk0eFONw">
                                        <div class="input-group-append">
                                            <!--Here start the formats select box-->
                                            <div class="ml-4">
                                                <select name="format_type" class="custom-select format-select form-control">
                                                    @foreach ($formats as $format)
                                                        <option value="{{$format->id}}">{{$format->description}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!--Here finish the formats select box-->
                                            <button type="submit" class="btn btn-outline-secondary format-button ml-1 pl-4 pr-4">Start!</button>
                                        </div>
                                    </div>
                                </form>
                                <!-- Here finish the input link of YT -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

        <!-- This is a blank space -->
    <div style="height:20px;"></div>
    <!-- Finish blank space -->

    <!-- Collapse -->
    <div class="row">
        <div class="col-md-5"></div>
        <div class="card custom-card">
            <div class="card-header custom-card-header">
                <form action=" {{url('/users', Auth::user()->id) }}" method="GET" >
                    <button type="submit" class="btn btn-outline-secondary ">Check downloads!</button>
                </form>
                @if(session('message'))
                    {{session('message')}}
                @endif
            </div>
        </div>
    </div>
    <!-- Final Collapse-->
        </div>
    </div>
</x-app-layout>
