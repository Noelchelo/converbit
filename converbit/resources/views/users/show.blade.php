<x-app-layout>
<x-slot name="header">
{{ header("Refresh:10") }}

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Downloads') }}
        </h2>
    </x-slot>
<div class="container">
    <div class="row">
    </div>
    <div style="height:25px;"></div>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="card cutom-card text-center">
                <div class="card-header custom-card-header">
                    Lists of downloads
                </div>
                <div id="showdiv" class="card-body custom-card-body">
                    <table class="table table-striped text-center">
                        <thead class="custom-thead">
                            <tr>
                                <th>Link</th>
                                <th>Format type</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody style="font-weight:bold;">
                            @foreach ($downloadsUser as $dUser)
                            <tr>
                                <td>{{$dUser->link}}</td>
                                <td>{{$dUser->description}}</td>
                                @if ($dUser->status == "Inactive")
                                <td>
                                    <button class="btn btn-danger" type="button" style="cursor:default;">
                                     {{$dUser->status}}
                                    </button>
                                </td>
                                @endif
                                @if ($dUser->status == "Active")
                                <td>
                                    <button class="btn btn-warning" type="button" style="cursor:default;">
                                        {{$dUser->status}}
                                    </button>
                                </td>
                                @endif
                                @if ($dUser->status == "Finished")
                                <td>
                                    <!-- <form action=" $dUser->link "> -->
                                        <a  class="btn btn-success" href="/download/{{$dUser->id}}" target="_blank">{{$dUser->status}}</a>
                                    <!--</form> -->
                                </td>
                                @endif
                                @if ($dUser->status == "Error")
                                <td>
                                    <button class="btn btn-warning" onclick="alert('Unable to convert video')" type="button" >
                                        {{$dUser->status}}
                                    </button>
                                </td>
                                @endif
                                <td>{{date('d-m-Y', strtotime($dUser->created_at))}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="dropdown-divider"></div>
                <div class="card-footer custom-card-footer">
                    {{ $downloadsUser->links() }}
                </div>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
</div>

</x-app-layout>