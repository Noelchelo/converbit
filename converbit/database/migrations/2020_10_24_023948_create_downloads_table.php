<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('downloads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('format_id');
            $table->string('link');
            $table->boolean('error')->default(0);
            $table->enum('status',['Inactive','Active','Finished','Error']);
            $table->string('location')->default("");
            $table->timestamp('updated_at')->nullable();;
            $table->timestamp('created_at')->nullable();;

            //$table->foreign('user_id')->references('id')->on('users');
            //$table->foreign('format_id')->references('id')->on('formats');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('downloads');
    }
}
