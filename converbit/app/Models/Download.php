<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    use HasFactory;

    protected $table = 'downloads';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'format_id',
        'link',
        'status',
        'location'
    ];

    public function getJson(){
        return json_encode([
            'id' => $this->id,
            'user_id' =>$this->user_id,
            'link' => $this->link,
            'format' => $this->format_id
        ]);
    }
}
