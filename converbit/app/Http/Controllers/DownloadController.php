<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Download;
use App\Models\File;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class DownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $d = new Download();
        $d->user_id = $user_id;
        $d->format_id = $request->input('format_type');
        $d->link = $request->input('link');
        if(!DB::table('downloads')->where('link',$d->link)->exists()){
        $d->status = 'Inactive';
        $d->save();
            $format_description = DB::table('formats')
                                ->where('id', $d->format_id)->first();
            $d->format_id = $format_description->description;
            $jsonD = $d->getJson();
            //This is to obtain the last page to redirect at once
            $downloadsUser = DB::table('downloads')
                                ->where('user_id', $user_id)
                                ->join('formats', 'downloads.format_id', '=', 'formats.id')
                                ->select('downloads.*','formats.description')
                                ->paginate(3);
            $url_to_redirect = '/dashboard';
            $this->sendJsonToBunnyMQ($jsonD);
            return redirect($url_to_redirect)->with('message','Download added successfully');
            
        }
        $d->link = $request->input('link');
        $d->location = DB::table('downloads')->where('link',$d->link)->first()->location;
        $d->status = 'Finished';
        $d->save();
        $url_to_redirect = '/dashboard';
        return redirect($url_to_redirect)->with('message','Download added successfully');
    }

    //RabbitMQ Method commented
    public function sendJsonToBunnyMQ($json)
    {
        $conn = new AMQPStreamConnection('192.168.0.10', 5672, 'admin', 'admin');
        $channel = $conn->channel();
        $channel->queue_declare('downloads', false, true, false, false);
        $message = new AMQPMessage($json);
        $channel->basic_qos(null, 1, null);
        $channel->basic_publish($message, '', 'downloads');
        $channel->close();
        $conn->close();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    public function download($id){
        $file = DB::table('downloads')->where('id', $id)->first();
        return redirect($file->location);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
