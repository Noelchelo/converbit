<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Download;

class UserController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::user()->id == $id) {
            $this->updateError();
            $userDB = DB::table('users')->where('id', $id)->first();
            $downloadsUser = DB::table('downloads')
                            ->where('user_id', $id)
                            ->join('formats', 'downloads.format_id', '=', 'formats.id')
                            ->select('downloads.*','formats.description')
                            ->paginate(3);
            return view('users.show', compact('userDB', 'downloadsUser'));
        }
        else {
            return 
            '<h1>Permission denied</h1><a href="/dashboard">dashboard</a>';
        }
    }

    private function updateError(){
        $downloads = Download::all();
        foreach ($downloads as $download) {
            if($download->error == 1){
                DB::table('downloads')
                ->where('id', $download->id)
                ->update(['status' => "Error"]);
            }
        }
    }
}
